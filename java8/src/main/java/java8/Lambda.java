package java8;

import java.util.List;
import java.util.stream.Stream;

import com.google.common.collect.ImmutableList;
import com.phanindra.domain.Person;

public class Lambda {
	public static void main(String args[]) {
		List<Person> people = ImmutableList.of(new Person().setName("Phanindra"), new Person().setName("Padmashree"));
		Stream<Person> filteredPeopleStrem = people.stream().filter(p -> p.getName().startsWith("P"));
		System.out.println(filteredPeopleStrem.count());
	}
}
