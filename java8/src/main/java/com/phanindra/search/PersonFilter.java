package com.phanindra.search;

import com.phanindra.domain.Person;

public interface PersonFilter {
	boolean filter(Person person);
}
