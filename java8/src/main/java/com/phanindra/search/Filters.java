package com.phanindra.search;

import java.util.List;
import java.util.function.Predicate;

import com.google.common.collect.Lists;
import com.phanindra.domain.Person;

public class Filters {
	public static List<Person> filter(List<Person> people, PersonFilter filter) {
		List<Person> filteredResult = Lists.newArrayList();
		for (Person person : people) {
			if (filter.filter(person)) {
				filteredResult.add(person);
			}
		}
		return filteredResult;
	}

	public static List<Person> filter(List<Person> people,
			Predicate<Person> predicate) {
		List<Person> filteredResult = Lists.newArrayList();
		for (Person person : people) {
			if (predicate.test(person)) {
				filteredResult.add(person);
			}
		}
		return filteredResult;
	}
}
