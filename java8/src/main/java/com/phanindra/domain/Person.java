package com.phanindra.domain;

import java8.A;
import java8.B;

public class Person implements A, B {

	private String name;

	private Status status;

	public String getName() {
		return name;
	}

	public Person setName(String name) {
		this.name = name;
		return this;
	}

	public Status getStatus() {
		return status;
	}

	public Person setStatus(Status status) {
		this.status = status;
		return this;
	}

	@Override
	public void foo() {
		System.out
				.println("Overriding to avoid duplicate default methods in parent interfaces");
	}
}
