package com.phanindra.domain;

import java.util.function.Predicate;

import static com.phanindra.domain.Status.MARRIED;

public class PersonPredicates {
	public static Predicate<Person> marriedPredicate() {
		return new Predicate<Person>() {
			@Override
			public boolean test(Person person) {
				return MARRIED.equals(person.getStatus());
			}
		};
	}

	public static Predicate<Person> marriedPredicateUsingLambda() {
		return p -> MARRIED.equals(p.getStatus());
	}
}
