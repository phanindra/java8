package com.phanindra.search;

import static com.phanindra.domain.PersonPredicates.marriedPredicate;
import static com.phanindra.domain.PersonPredicates.marriedPredicateUsingLambda;
import static com.phanindra.domain.Status.MARRIED;
import static com.phanindra.domain.Status.SINGLE;
import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import com.google.common.collect.ImmutableList;
import com.phanindra.domain.Person;
import com.phanindra.domain.Status;

public class PersonFilterTest {

	public static final List<Person> PEOPLE = ImmutableList.of(
			person("Phanindra", MARRIED), person("Padmashree", MARRIED),
			person("Pranay", SINGLE));

	@Test
	public void writeYourOwnFunctionalInterfaceApproach() {
		List<Person> marriedPeople = Filters.filter(PEOPLE, new PersonFilter() {
			@Override
			public boolean filter(Person person) {
				return MARRIED.equals(person.getStatus());
			}
		});
		assertEquals(2, marriedPeople.size());
	}

	@Test
	public void jdkPredicatesApproach() {
		List<Person> marriedPeople = Filters.filter(PEOPLE, marriedPredicate());
		assertEquals(2, marriedPeople.size());
	}

	@Test
	public void predicatesUsingLambda() {
		List<Person> marriedPeople = Filters.filter(PEOPLE,
				marriedPredicateUsingLambda());
		assertEquals(2, marriedPeople.size());
	}

	@Test
	public void usingStreams() {
		assertEquals(2, PEOPLE.stream().filter(marriedPredicateUsingLambda())
				.count());
	}

	private static Person person(String name, Status status) {
		return new Person().setName(name).setStatus(status);
	}

}
